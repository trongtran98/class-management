DROP DATABASE IF EXISTS class_manager;
CREATE DATABASE IF NOT EXISTS class_manager;

USE class_manager;

DROP TABLE IF EXISTS roles;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS decentralization;
DROP TABLE IF EXISTS teachers;
DROP TABLE IF EXISTS students;
DROP TABLE IF EXISTS subjects;
DROP TABLE IF EXISTS classroom;
DROP TABLE IF EXISTS classes;
DROP TABLE IF EXISTS classes_students;
DROP TABLE IF EXISTS homeworks;
DROP TABLE IF EXISTS notify;
DROP TABLE IF EXISTS posts;
DROP TABLE IF EXISTS comments;
DROP TABLE IF EXISTS likes;
DROP TABLE IF EXISTS marks;
DROP TABLE IF EXISTS documents;
DROP TABLE IF EXISTS project;

CREATE TABLE IF NOT EXISTS roles ( -- quyền
  id VARCHAR(255),
  role_name VARCHAR(255),
  PRIMARY KEY (id)
) engine=InnoDB;

CREATE TABLE IF NOT EXISTS users (
  user_name varchar(255),
  user_password varchar(255),
  full_name VARCHAR(255),
  user_email VARCHAR(255),
  user_phone VARCHAR(255),
  user_avatar VARCHAR(255),
  user_address VARCHAR(255),
  user_gender VARCHAR(255),
  user_dob DATE,
  PRIMARY KEY (user_name)
) engine=InnoDB;

CREATE TABLE IF NOT EXISTS decentralization ( -- phân quyền
  id BIGINT NOT NULL auto_increment,
  user_name varchar(255),
  role_id VARCHAR(255),
  PRIMARY KEY (id),
  CONSTRAINT FK_role FOREIGN KEY (role_id) REFERENCES roles(id),
  CONSTRAINT FK_username FOREIGN KEY (user_name) REFERENCES users(user_name)
) engine=InnoDB;

CREATE TABLE IF NOT EXISTS teachers (
  id VARCHAR(255),
  academic_rank VARCHAR(255), -- học hàm
  dest VARCHAR(255), -- mô tả
  user_name varchar(255),
  PRIMARY KEY (id),
  CONSTRAINT user_name_teacher_fk FOREIGN KEY (user_name) REFERENCES users(user_name)
) engine=InnoDB;

CREATE TABLE IF NOT EXISTS students (
  id VARCHAR(255),
  user_name varchar(255),
  day_admission date, -- ngày nhập học
  PRIMARY KEY (id),
  CONSTRAINT fk_user_name_student FOREIGN KEY (user_name) REFERENCES users(user_name)
) engine=InnoDB;

CREATE TABLE IF NOT EXISTS subjects (
  id VARCHAR(255),
  subject_name VARCHAR(255),
  status varchar(255),
  PRIMARY KEY (id)
) engine=InnoDB;

CREATE TABLE IF NOT EXISTS classroom (
  id BIGINT NOT NULL auto_increment,
  classroom_name varchar(255),
   PRIMARY KEY (id)
) engine=InnoDB;

CREATE TABLE IF NOT EXISTS classes (
  id VARCHAR(255),
  dest VARCHAR(255),
  create_date datetime,
  end_date datetime,
  classroom_id BIGINT,
  school_day varchar(255), -- Ngày học để dạng json có thứ(thứ hai, thứ ba,..và tiết học< tiết 1(7h->8h), ...)
  school_shift varchar(255), -- ca học
  status varchar(255),
  teacher_id VARCHAR(255),
  subject_id VARCHAR(255),
  PRIMARY KEY (id),
  CONSTRAINT teacher_class_fk FOREIGN KEY (teacher_id) REFERENCES teachers(id),
  CONSTRAINT subject_class_fk FOREIGN KEY (subject_id) REFERENCES subjects(id),
  CONSTRAINT classroom_fk FOREIGN KEY (classroom_id) REFERENCES classroom(id)

) engine=InnoDB;

CREATE TABLE IF NOT EXISTS classes_students ( -- bảng quan hệ n-n giữa lớp học và sinh viên
  id BIGINT NOT NULL auto_increment,
  class_id VARCHAR(255),
  student_id VARCHAR(255),
  PRIMARY KEY (id),
  CONSTRAINT class_student_fk FOREIGN KEY (class_id) REFERENCES classes(id),
  CONSTRAINT student_id_fk FOREIGN KEY (student_id) REFERENCES students(id)
) engine=InnoDB;

CREATE TABLE IF NOT EXISTS homeworks(
  id BIGINT NOT NULL auto_increment,
  create_date datetime,
  file_name VARCHAR(255),
  file_real_name varchar(255),
  student_id VARCHAR(255),
  class_id VARCHAR(255),
  PRIMARY KEY (id),
  CONSTRAINT hw_student_fk FOREIGN KEY (student_id) REFERENCES students(id),
  CONSTRAINT hw_class_fk FOREIGN KEY (class_id) REFERENCES classes(id)
) engine=InnoDB;

CREATE TABLE IF NOT EXISTS notify(
  id BIGINT NOT NULL auto_increment,
  content text,
  status VARCHAR(255),
  sender VARCHAR(255),
  title VARCHAR(255),
  time datetime,
  teacher_id VARCHAR(255),
  student_id VARCHAR(255),
  class_id VARCHAR(255),
  PRIMARY KEY (id),
  CONSTRAINT teacher_notify_fk FOREIGN KEY (teacher_id) REFERENCES teachers(id),
  CONSTRAINT student_notify_fk FOREIGN KEY (student_id) REFERENCES students(id),
  CONSTRAINT class_notify_fk FOREIGN KEY (class_id) REFERENCES classes(id)
) engine=InnoDB;

CREATE TABLE IF NOT EXISTS posts
(
    id bigint PRIMARY KEY AUTO_INCREMENT,
    content longtext NOT NULL,
    file_name varchar(255),
    file_real_name varchar(255),
    user_name varchar(255),
    status int DEFAULT 1 NOT NULL,
    time datetime,
    class_id varchar(255),
    CONSTRAINT class_fk FOREIGN KEY (class_id) REFERENCES classes(id),
    CONSTRAINT user_name_fk FOREIGN KEY (user_name) REFERENCES users(user_name)
) engine=InnoDB;

CREATE TABLE IF NOT EXISTS comments
(
    id bigint PRIMARY KEY AUTO_INCREMENT,
    content longtext NOT NULL,
    post_id bigint,
    user_name varchar(255),
    time datetime,
    CONSTRAINT postid_fk FOREIGN KEY (post_id) REFERENCES posts (id),
    CONSTRAINT user_name_comment_fk FOREIGN KEY (user_name) REFERENCES users (user_name)
) engine=InnoDB;

CREATE TABLE IF NOT EXISTS likes
(
    id bigint PRIMARY KEY AUTO_INCREMENT,
    post_id bigint,
    user_name varchar(255),
    CONSTRAINT like_postid_fk FOREIGN KEY (post_id) REFERENCES posts (id),
    CONSTRAINT like_user_user_name_fk FOREIGN KEY (user_name) REFERENCES users (user_name)
) engine=InnoDB;

CREATE TABLE IF NOT EXISTS marks (
  id BIGINT NOT NULL auto_increment,
  theory double ,
  practice double,
  end_of_period double,
  student_id VARCHAR(255),
  teacher_id VARCHAR(255),
  subject_id VARCHAR(255),
  class_id VARCHAR(255),
  PRIMARY KEY (id),
  CONSTRAINT FK_student FOREIGN KEY (student_id) REFERENCES students(id),
  CONSTRAINT FK_teacher FOREIGN KEY (teacher_id) REFERENCES teachers(id),
  CONSTRAINT FK_subject FOREIGN KEY (subject_id) REFERENCES subjects(id),
  CONSTRAINT FK_class FOREIGN KEY (class_id) REFERENCES classes(id)
) engine=InnoDB;

CREATE TABLE IF NOT EXISTS documents
(
    id bigint PRIMARY KEY AUTO_INCREMENT,
    dest varchar(255),
    class_id varchar(255),
    file_name varchar(255),
    file_real_name varchar(255),
     time datetime,
    CONSTRAINT document_class_fk FOREIGN KEY (class_id) REFERENCES classes (id)
);

CREATE TABLE IF NOT EXISTS project
(
  id bigint PRIMARY KEY AUTO_INCREMENT,
  content text,
  create_date datetime,
  due_date datetime,
  class_id varchar(255),
  teacher_id varchar(255),
  status varchar(255),
  CONSTRAINT FK_hw_class FOREIGN KEY (class_id) REFERENCES classes(id),
  CONSTRAINT FK_hw_teacher FOREIGN KEY (teacher_id) REFERENCES teachers(id)
);