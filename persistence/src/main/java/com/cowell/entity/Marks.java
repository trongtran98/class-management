package com.cowell.entity;

import javax.persistence.*;

/**
 * Posted from Dec 02, 2018, 11:23 PM
 *
 * @author Vi Quynh (vi.quynh.31598@gmail.com)
 */

@Entity
@Table(name = "marks")
public class Marks {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "theory")
    private Float theory;

    @Column(name = "practice")
    private Float practice;

    @Column(name = "end_of_period")
    private Float endPeriod;

    @ManyToOne
    @JoinColumn(name = "student_id")
    private Students student;

    @ManyToOne
    @JoinColumn(name = "class_id")
    private Classes classes;

    @ManyToOne
    @JoinColumn(name = "teacher_id")
    private Teachers teacher;

    @ManyToOne
    @JoinColumn(name = "subject_id")
    private Subjects subject;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Float getTheory() {
        return theory;
    }

    public void setTheory(Float theory) {
        this.theory = theory;
    }

    public Float getPractice() {
        return practice;
    }

    public void setPractice(Float practice) {
        this.practice = practice;
    }

    public Float getEndPeriod() {
        return endPeriod;
    }

    public void setEndPeriod(Float endPeriod) {
        this.endPeriod = endPeriod;
    }

    public Students getStudent() {
        return student;
    }

    public void setStudent(Students student) {
        this.student = student;
    }

    public Classes getClasses() {
        return classes;
    }

    public void setClasses(Classes classes) {
        this.classes = classes;
    }

    public Teachers getTeacher() {
        return teacher;
    }

    public void setTeacher(Teachers teacher) {
        this.teacher = teacher;
    }

    public Subjects getSubject() {
        return subject;
    }

    public void setSubject(Subjects subject) {
        this.subject = subject;
    }
}
