package com.cowell.entity;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

/**
 * Posted from Dec 02, 2018, 11:22 PM
 *
 * @author Vi Quynh (vi.quynh.31598@gmail.com)
 */

@Entity
@Table(name = "documents")
public class Documents {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "dest")
    private String dest;

    @Column(name = "file_name")
    private String fileName;

    @Column(name="file_real_name")
    private String fileRealName;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="time")
    private Date time;

    @ManyToOne
    @JoinColumn(name = "class_id")
    private Classes classes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDest() {
        return dest;
    }

    public void setDest(String dest) {
        this.dest = dest;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileRealName() {
        return fileRealName;
    }

    public void setFileRealName(String fileRealName) {
        this.fileRealName = fileRealName;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Classes getClasses() {
        return classes;
    }

    public void setClasses(Classes classes) {
        this.classes = classes;
    }
}
